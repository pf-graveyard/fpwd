/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/**
 * Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * fpwd — hash-based (SHA-3) password generator and manager
 */

#pragma once

#ifndef __FPWD_ENTITY_H__
#define __FPWD_ENTITY_H__

#include <string>

#include "fpwdSyms.h"
#include "fpwdURI.h"

using std::string;

class fpwdEntity
{
	public:
		fpwdEntity(void) {};
		fpwdEntity(fpwdURI _uri,
				unsigned int _version,
				unsigned int _length,
				fpwdSyms _syms);
		string toString(void);
		string getPassword(const string& _master_password);
		fpwdURI getURI(void) { return this->uri; };
		unsigned int getVersion(void) { return this->version; };
		unsigned int getLength(void) { return this->length; };
		fpwdSyms getSyms(void) { return this->syms; };
		void setProtocol(const string& _protocol) { this->uri.setProtocol(_protocol); };
		void setLogin(const string& _login) { this->uri.setLogin(_login); };
		void setServer(const string& _server) { this->uri.setServer(_server); };
		void setPort(const int& _port) { this->uri.setPort(_port); };
		void setResource(const string& _resource) { this->uri.setResource(_resource); };
		void setVersion(const unsigned int& _version) { this->version = _version; };
		void setLength(const unsigned int& _length) { this->length = _length; };
		void setSymsaz(const bool& _syms_az) { this->syms.setSymsaz(_syms_az); };
		void setSymsAZ(const bool& _syms_AZ) { this->syms.setSymsAZ(_syms_AZ); };
		void setSyms09(const bool& _syms_09) { this->syms.setSyms09(_syms_09); };
		void setSymsSpecial(const bool& _syms_special) { this->syms.setSymsSpecial(_syms_special); };
	private:
		fpwdURI uri;
		unsigned int version;
		unsigned int length;
		fpwdSyms syms;
};

#endif /* __FPWD_ENTITY_H__ */


/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/**
 * Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * fpwd — hash-based (SHA-3) password generator and manager
 */

#pragma once

#ifndef __FPWD_URI_H__
#define __FPWD_URI_H__

#include <string>

using std::string;

class fpwdURI
{
	public:
		fpwdURI(void) {};
		fpwdURI(string _protocol,
				string _login,
				string _server,
				int _port,
				string _resource):
		protocol(_protocol),
		login(_login),
		server(_server),
		port(_port),
		resource(_resource) {};
		string getURI(string _master_password);
		string getFormattedURI(void);
		string getProtocol(void) { return this->protocol; };
		string getLogin(void) { return this->login; };
		string getServer(void) { return this->server; };
		int getPort(void) { return this->port; };
		string getResource(void) { return this->resource; };
		void setProtocol(const string& _protocol) { this->protocol = _protocol; };
		void setLogin(const string& _login) { this->login = _login; };
		void setServer(const string& _server) { this->server = _server; };
		void setPort(const int& _port) { this->port = _port; };
		void setResource(const string& _resource) { this->resource = _resource; };
	private:
		string protocol;
		string login;
		string server;
		int port;
		string resource;
};

#endif /* __FPWD_URI_H__ */


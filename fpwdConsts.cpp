/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/**
 * Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * fpwd — hash-based (SHA-3) password generator and manager
 */

#include <string>
#include <vector>

#include "fpwdConsts.h"

using std::string;
using std::vector;

const vector<string> fpwdConsts::salts
{
	"az",
	"AZ",
	"09",
	"special1",
	"special2",
	"special3",
	"special4",
	"fisher yates shuffle"
};

const vector<std::pair<int, int>> fpwdConsts::ascii_map
{{
	{'z' - 'a' + 1, 'a'},	// a..z
	{'Z' - 'A' + 1, 'A'},	// A..Z
	{'9' - '0' + 1, '0'},	// 0..9
	{'/' - '!' + 1, '!'},	// !../
	{'@' - ':' + 1, ':'},	// :..@
	{'`' - '[' + 1, '['},	// [..`
	{'~' - '{' + 1, '{'}	// {..~
}};


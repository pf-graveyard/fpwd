/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/**
 * Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * fpwd — hash-based (SHA-3) password generator and manager
 */

#include <array>
#include <stdint.h>
#include <string>

#include "fpwdConsts.h"
#include "fpwdEntity.h"
#include "fpwdSyms.h"
#include "fpwdURI.h"
#include <keccak/keccak.h>

using std::string;
using std::array;

fpwdEntity::fpwdEntity(fpwdURI _uri,
				unsigned int _version,
				unsigned int _length,
				fpwdSyms _syms):
		uri(_uri),
		version(_version),
		length(_length),
		syms(_syms)
{
}

string fpwdEntity::toString(void)
{
	return this->uri.getFormattedURI();
}

string fpwdEntity::getPassword(const string& _master_password)
{
	array<string, SALTS_SIZE> pre_hash;
	array<string, ASCII_MAP_SIZE> pwd;
	string res;
	array<array<uint8_t, KECCAK_SIZE>, SALTS_SIZE> md;
	array<unsigned int, ASCII_MAP_SIZE> index;

	for (unsigned int i = 0; i < pwd.size(); i++)
		index[i] = 0;

	string base =
			this->uri.getURI(_master_password) +
			std::to_string(this->version) +
			std::to_string(this->length) +
			this->syms.toTextSequence();
	for (unsigned int i = 0; i < pre_hash.size(); i++)
		pre_hash[i] = base + fpwdConsts::salts[i];

	for (unsigned int i = 0; i < pre_hash.size(); i++)
		keccak((uint8_t*)pre_hash[i].c_str(), pre_hash[i].length(), md[i].data(), KECCAK_SIZE);

	for (unsigned int i = 0; i < this->length; i++)
		for (unsigned int j = 0; j < pwd.size(); j++)
			pwd[j] += md[j][i] % fpwdConsts::ascii_map[j].first + fpwdConsts::ascii_map[j].second;

	if (this->syms.atLeastOne())
	{
		unsigned int filled = 0;
		unsigned int index_sym = 0;
		unsigned int index_special = 0;

		while (filled < this->length)
		{
			if (this->syms.toBoolVector()[index_sym])
			{
				if (index_sym == SYMS_AMOUNT - 1)
				{
					res += pwd[index_sym + index_special][index[index_sym + index_special]++];
					index_special++;
					if (index_special > SYMS_AMOUNT - 1)
						index_special = 0;
				} else
					res += pwd[index_sym][index[index_sym]++];
				filled++;
			}
			index_sym++;
			if (index_sym > SYMS_AMOUNT - 1)
				index_sym = 0;
		}

		for (unsigned int i = this->length - 1; i >= 1; i--)
		{
			int j = md[SALTS_SIZE - 1][i] % (i + 1);
			char tmp = res[i];
			res[i] = res[j];
			res[j] = tmp;
		}
	}

	return res;
}


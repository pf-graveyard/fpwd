/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/**
 * Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * fpwd — hash-based (SHA-3) password generator and manager
 */

#include <string>
#include <termios.h>
#include <unistd.h>

#include "fpwdTools.h"

using std::string;

string fpwdTools::getPassword(void)
{
	string ret;
	struct termios oldt, newt;
	int c;

	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;

	newt.c_lflag &= ~(ECHO);

	tcsetattr(STDIN_FILENO, TCSANOW, &newt);

	while ((c = getchar()) != '\n' && c != EOF)
		ret += c;

	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

	return ret;
}


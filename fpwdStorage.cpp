/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/**
 * Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * fpwd — hash-based (SHA-3) password generator and manager
 */

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <iostream>
#include <string>
#include <vector>

#include "fpwdEntity.h"
#include "fpwdStorage.h"

using std::string;
using std::vector;

fpwdStorage::fpwdStorage(string _file)
{
	this->file = _file;
	boost::property_tree::ini_parser::read_ini(this->file, this->ini);
}

fpwdEntity fpwdStorage::getEntityById(int _id)
{
	fpwdEntity ret;

	string section = std::to_string(_id);

	string value_protocol(this->ini.get<string>(section + ".protocol"));
	string value_login(this->ini.get<string>(section + ".login"));
	string value_server(this->ini.get<string>(section + ".server"));
	string value_port(this->ini.get<string>(section + ".port"));
	string value_resource(this->ini.get<string>(section + ".resource"));
	string value_version(this->ini.get<string>(section + ".version"));
	string value_length(this->ini.get<string>(section + ".length"));
	string value_syms_az(this->ini.get<string>(section + ".syms_az"));
	string value_syms_AZ(this->ini.get<string>(section + ".syms_AZ"));
	string value_syms_09(this->ini.get<string>(section + ".syms_09"));
	string value_syms_special(this->ini.get<string>(section + ".syms_special"));

	fpwdURI uri(value_protocol,
				value_login,
				value_server,
				std::stoi(value_port),
				value_resource);
	fpwdSyms syms
	{{
		{"az", value_syms_az == "true"},
		{"AZ", value_syms_AZ == "true"},
		{"09", value_syms_09 == "true"},
		{"special", value_syms_special == "true"},
	}};

	ret = fpwdEntity(uri,
					std::stoi(value_version),
					std::stoi(value_length),
					syms);

	return ret;
}

vector<std::pair<int, fpwdEntity>> fpwdStorage::grep(const string& _site)
{
	vector<std::pair<int, fpwdEntity>> ret;

	for (auto& i: this->ini)
	{
		string section = i.first + ".server";
		string server = this->ini.get<string>(section);
		if (server.find(_site, 0) != string::npos || _site == "")
		{
			int id_i = std::stoi(i.first);
			ret.push_back({id_i, this->getEntityById(id_i)});
		}
	}

	return ret;
}

int fpwdStorage::getMaxId(void)
{
	vector<std::pair<int, fpwdEntity>> entries = this->grep("");
	int max;

	if (entries.size() > 0)
		max = entries[0].first;
	else
		return -1;

	for (unsigned int i = 1; i < entries.size(); i++)
		if (entries[i].first > max)
			max = entries[i].first;

	return max;
}

int fpwdStorage::store(fpwdEntity _entity)
{
	int id = this->getMaxId() + 1;

	this->replace(id, _entity);

	return id;
}

void fpwdStorage::replace(int _id, fpwdEntity _entity)
{
	string section(std::to_string(_id));

	this->ini.put(section + ".protocol", _entity.getURI().getProtocol());
	this->ini.put(section + ".login", _entity.getURI().getLogin());
	this->ini.put(section + ".server", _entity.getURI().getServer());
	this->ini.put(section + ".port", _entity.getURI().getPort());
	this->ini.put(section + ".resource", _entity.getURI().getResource());
	this->ini.put(section + ".version", _entity.getVersion());
	this->ini.put(section + ".length", _entity.getLength());
	this->ini.put(section + ".syms_az", _entity.getSyms().getSyms()["az"]);
	this->ini.put(section + ".syms_AZ", _entity.getSyms().getSyms()["AZ"]);
	this->ini.put(section + ".syms_09", _entity.getSyms().getSyms()["09"]);
	this->ini.put(section + ".syms_special", _entity.getSyms().getSyms()["special"]);

	return;
}

void fpwdStorage::commit(void)
{
	boost::property_tree::ini_parser::write_ini(this->file, this->ini);
}


/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/**
 * Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * fpwd — hash-based (SHA-3) password generator and manager
 */

#pragma once

#ifndef __FPWD_CONSTS_H__
#define __FPWD_CONSTS_H__

#include <string>
#include <vector>

#include "fpwdConsts.h"

using std::string;
using std::vector;

#define ASCII_MAP_SIZE	7
#define KECCAK_SIZE		64
#define SALTS_SIZE		8
#define SYMS_AMOUNT		4

enum fpwdCmd
{
	CMD_NOOP,
	CMD_LIST,
	CMD_GREP,
	CMD_GET,
	CMD_ADD,
	CMD_SET
};

class fpwdConsts
{
	public:
		static const vector<string> salts;
		static const vector<std::pair<int, int>> ascii_map;
};

#endif /* __FPWD_CONSTS_H__ */


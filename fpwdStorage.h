/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/**
 * Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * fpwd — hash-based (SHA-3) password generator and manager
 */

#pragma once

#ifndef __FPWD_STORAGE_H__
#define __FPWD_STORAGE_H__

#include <boost/property_tree/ptree.hpp>
#include <vector>

#include "fpwdEntity.h"
#include "fpwdStorage.h"

using std::vector;

class fpwdStorage
{
	public:
		fpwdStorage(string _file);
		fpwdEntity getEntityById(int _id);
		vector<std::pair<int, fpwdEntity>> grep(const string& _site);
		int getMaxId(void);
		int store(fpwdEntity _entity);
		void replace(int _id, fpwdEntity _entity);
		void commit(void);
	private:
		string file;
		boost::property_tree::ptree ini;
};

#endif /* __FPWD_STORAGE_H__ */


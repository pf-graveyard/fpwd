/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/**
 * Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * fpwd — hash-based (SHA-3) password generator and manager
 */

#include <map>
#include <string>
#include <vector>

#include "fpwdConsts.h"
#include "fpwdSyms.h"

using std::string;
using std::vector;

string fpwdSyms::toTextSequence(void)
{
	return
			std::to_string(this->syms["az"]) +
			std::to_string(this->syms["AZ"]) +
			std::to_string(this->syms["09"]) +
			std::to_string(this->syms["special"]);
}

vector<bool> fpwdSyms::toBoolVector(void)
{
	return vector<bool>
	{
		this->syms["az"],
		this->syms["AZ"],
		this->syms["09"],
		this->syms["special"]
	};
}

bool fpwdSyms::atLeastOne(void)
{
	return
			this->syms["az"] ||
			this->syms["AZ"] ||
			this->syms["09"] ||
			this->syms["special"];
}


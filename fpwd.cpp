/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/**
 * Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * fpwd — hash-based (SHA-3) password generator and manager
 */

#include <iostream>
#include <stdlib.h>
#include <sysexits.h>
#include <tclap/CmdLine.h>
#include <vector>

#include "fpwdEntity.h"
#include "fpwdStorage.h"
#include "fpwdSyms.h"
#include "fpwdTools.h"
#include "fpwdURI.h"

using std::cout;
using std::string;
using std::vector;

int main(int argc, char** argv)
{
	int id_get = -1;
	int id_set = -1;
	fpwdCmd cmd = CMD_NOOP;
	string storage_path;
	string to_grep = "";
	string protocol;
	string login;
	string server;
	unsigned int port = 0;
	string resource;
	unsigned int version = 1;
	unsigned int length = 20;
	bool use_az = true;
	bool use_AZ = true;
	bool use_09 = true;
	bool use_special = true;

	TCLAP::CmdLine arg_cmd("fpwd — hash-based (SHA-3) password generator and manager", ' ', "0.1");
	TCLAP::ValueArg<string> arg_storage("s", "storage",
					"Path to fPwd storage", false,
					string(std::getenv("HOME")) + "/.config/fastpwd/urls.conf",
					"string", arg_cmd);
	TCLAP::SwitchArg arg_list("L", "list",
					"List the storage",
					arg_cmd, false);
	TCLAP::ValueArg<string> arg_grep("G", "grep",
					"Grep the storage", false,
					"",
					"string", arg_cmd);
	TCLAP::ValueArg<int> arg_get("E", "get",
					"Get item from the storage", false,
					-1,
					"int", arg_cmd);
	TCLAP::SwitchArg arg_add("A", "add",
					"Add item to the storage",
					arg_cmd, false);
	TCLAP::ValueArg<int> arg_set("S", "set",
					"Change item in the storage", false,
					-1,
					"int", arg_cmd);
	TCLAP::ValueArg<string> arg_protocol("p", "protocol",
					"URI protocol", false,
					"local",
					"string", arg_cmd);
	TCLAP::ValueArg<string> arg_login("l", "login",
					"URI login", false,
					"",
					"string", arg_cmd);
	TCLAP::ValueArg<string> arg_server("e", "server",
					"URI server", false,
					"",
					"string", arg_cmd);
	TCLAP::ValueArg<unsigned int> arg_port("o", "port",
					"URI port", false,
					0,
					"unsigned int", arg_cmd);
	TCLAP::ValueArg<string> arg_resource("r", "resource",
					"URI resource", false,
					"/",
					"string", arg_cmd);
	TCLAP::ValueArg<unsigned int> arg_version("i", "password-version",
					"Password version", false,
					1,
					"unsigned int", arg_cmd);
	TCLAP::ValueArg<unsigned int> arg_length("n", "password-length",
					"Password length", false,
					20,
					"unsigned int", arg_cmd);
	TCLAP::SwitchArg arg_az("1", "use-az",
					"Use a..z",
					arg_cmd, false);
	TCLAP::SwitchArg arg_AZ("2", "use-AZ",
					"Use A..Z",
					arg_cmd, false);
	TCLAP::SwitchArg arg_09("3", "use-09",
					"Use 0..9",
					arg_cmd, false);
	TCLAP::SwitchArg arg_special("4", "use-special",
					"Use special characters",
					arg_cmd, false);

	arg_cmd.parse(argc, argv);

	storage_path = arg_storage.getValue();
	if (arg_list.getValue())
		cmd = CMD_LIST;

	to_grep = arg_grep.getValue();
	if (to_grep != "")
		cmd = CMD_GREP;

	id_get = arg_get.getValue();
	if (id_get >= 0)
		cmd = CMD_GET;

	if (arg_add.getValue())
		cmd = CMD_ADD;

	id_set = arg_set.getValue();
	if (id_set >= 0)
		cmd = CMD_SET;

	protocol = arg_protocol.getValue();
	login = arg_login.getValue();
	server = arg_server.getValue();
	port = arg_port.getValue();
	resource = arg_resource.getValue();
	version = arg_version.getValue();
	length = arg_length.getValue();
	use_az = arg_az.getValue();
	use_AZ = arg_AZ.getValue();
	use_09 = arg_09.getValue();
	use_special = arg_special.getValue();

	fpwdStorage storage(storage_path);
	vector<std::pair<int, fpwdEntity>> entries;
	fpwdEntity entity;
	string password;
	string master_password;
	fpwdURI new_URI;
	fpwdSyms new_syms;
	fpwdEntity new_entity;

	switch (cmd)
	{
		case CMD_NOOP:
			break;
		case CMD_LIST:
			entries = storage.grep("");
			for (unsigned int i = 0; i < entries.size(); i++)
				cout << entries[i].first << ": " << entries[i].second.toString() << "\n";
			break;
		case CMD_GREP:
			entries = storage.grep(to_grep);
			for (unsigned int i = 0; i < entries.size(); i++)
				cout << entries[i].first << ": " << entries[i].second.toString() << "\n";
			break;
		case CMD_GET:
			entity = storage.getEntityById(id_get);
			cout << "Enter master password and press Enter: ";
			master_password = fpwdTools::getPassword();
			password = entity.getPassword(master_password);
			cout << "\n" << "Desired password is: " << password << "\n";
			break;
		case CMD_ADD:
			new_URI = fpwdURI(protocol, login, server, port, resource);
			new_syms =
			{{
				{"az", use_az},
				{"AZ", use_AZ},
				{"09", use_09},
				{"special", use_special}
			}};
			new_entity = fpwdEntity(new_URI, version, length, new_syms);
			cout << "Stored as entity with ID: " << storage.store(new_entity) << "\n";
			storage.commit();
			break;
		case CMD_SET:
			entity = storage.getEntityById(id_set);
			if (arg_protocol.isSet())
				entity.setProtocol(protocol);
			if (arg_login.isSet())
				entity.setLogin(login);
			if (arg_server.isSet())
				entity.setServer(server);
			if (arg_port.isSet())
				entity.setPort(port);
			if (arg_resource.isSet())
				entity.setResource(resource);
			if (arg_version.isSet())
				entity.setVersion(version);
			if (arg_length.isSet())
				entity.setLength(length);
			if (arg_az.isSet())
				entity.setSymsaz(use_az);
			if (arg_AZ.isSet())
				entity.setSymsAZ(use_az);
			if (arg_09.isSet())
				entity.setSyms09(use_09);
			if (arg_special.isSet())
				entity.setSymsSpecial(use_special);
			storage.replace(id_set, entity);
			storage.commit();
			break;
		default:
			break;
	}

	exit(EX_OK);
}


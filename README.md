fpwd
====

Hash-based (SHA-3) password generator and manager.

Description
-----------

fpwd generates password based on master password (private and never stored) and resource information.

Resource information is:

* access protocol
* user login
* server name or address
* port
* resource path
* password version
* password symbol sets
* password length

Compiling
---------

### Prerequisites

* cmake (tested with 3.1.3)
* C++ compiler (tested with g++ 4.9.2, clang 3.5.1, icc 15.0.1)
* make (tested with GNU Make 4.0)
* iniparser library (tested with v3.1)
* tclap library (tested with v1.2.1)

### Compiling

In source tree root create `build` folder, enter it and type the following command to generate make file:

`cmake ..`

Then compile the whole project:

`make -jN`

where N is machine CPU count. As a result `fpwd` executable should be produced.

Distribution and Contribution
-----------------------------

fpwd is provided under terms and conditions of GPLv3.
See file "COPYING" for details.
Mail any suggestions, bugreports and comments to me: oleksandr@natalenko.name.

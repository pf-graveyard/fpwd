/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/**
 * Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * fpwd — hash-based (SHA-3) password generator and manager
 */

#pragma once

#ifndef __FPWD_SYMS_H__
#define __FPWD_SYMS_H__

#include <map>
#include <string>
#include <vector>

#include "fpwdConsts.h"
#include "fpwdSyms.h"

using std::string;
using std::vector;

class fpwdSyms
{
	public:
		fpwdSyms() {};
		fpwdSyms(std::map<string, bool> _syms): syms(_syms) {};
		string toTextSequence(void);
		vector<bool> toBoolVector(void);
		bool atLeastOne(void);
		std::map<string, bool> getSyms(void) { return this->syms; };
		void setSymsaz(const bool& _syms_az) { this->syms["az"] = _syms_az; };
		void setSymsAZ(const bool& _syms_AZ) { this->syms["AZ"] = _syms_AZ; };
		void setSyms09(const bool& _syms_09) { this->syms["09"] = _syms_09; };
		void setSymsSpecial(const bool& _syms_special) { this->syms["special"] = _syms_special; };
	private:
		std::map<string, bool> syms;
};

#endif /* __FPWD_SYNS_H__ */

